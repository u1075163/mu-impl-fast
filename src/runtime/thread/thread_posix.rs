// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libc;

pub use super::*;
pub use ast::ir::*;
//pub use ast::ir_rt::*;
//pub use ast::ir_rt_posix::*;

pub use std::mem;
pub use std::os::raw::c_void;
pub use std::ptr;
//use rodal::Address;

pub type SysThreadID = libc::pthread_t;
pub type SysThreadAttr = libc::pthread_attr_t;
pub type SysResult = libc::c_int;
pub type SysVoid = libc::c_void;

pub const MU_SUCCESS: SysResult = 0;
pub const SYSNULL: *const i32 = ptr::null();

#[derive(Debug)]
#[repr(C)]
pub(super) struct MuThreadContext {
    pub muthread: Address,
    //    attr: Option<RTAttr>,
    pub new_sp: Address,
    pub exception: Address
}

pub(super) extern "C" fn _pthread_entry_point(
    _arg: *mut SysVoid
) -> *mut SysVoid {
    //    let muthread_ptr = _arg as *mut MuThread;
    let muthread_boxed: Box<MuThread> =
        unsafe { Box::from_raw(_arg as *mut MuThread) };
    debug!("PTHREAD_ENTRY THREAD: {}", muthread_boxed);
    //    let muthread_ptr = Box::into_raw(context);
    //    let context = unsafe { Box::from_raw(context) };

    let context = unsafe {
        Box::from_raw(muthread_boxed.sys_thread_context as *mut MuThreadContext)
    };

    debug!("PTHREAD_ENTRY input context:{:#?}", context);
    let _muthread = Box::into_raw(muthread_boxed);

    let muthread_ptr: *mut MuThread = context.muthread.to_ptr_mut();
    let new_sp = context.new_sp;
    let exception = context.exception;

    let _context = Box::into_raw(context);

    let exception = {
        if exception == Address::from_ptr(std::ptr::null() as *const Address) {
            None
        } else {
            Some(exception)
        }
    };

    //    let muth_ptr = Box::into_raw(muthread);
    //
    //    let muthread = unsafe { Box::from_raw(muth_ptr) };

    //    let muthread = Box::into_raw(muthread);
    //    let muthread = muthread as *const MuThread as *mut MuThread;
    // set thread local
    unsafe { set_thread_local(muthread_ptr) };

    let addr = unsafe { muentry_get_thread_local() };
    let sp_threadlocal_loc = addr + *NATIVE_SP_LOC_OFFSET;
    debug!("new sp: 0x{:x}", new_sp);
    debug!("sp_store: 0x{:x}", sp_threadlocal_loc);

    unsafe {
        match exception {
            Some(e) => {
                muthread_start_exceptional(e, new_sp, sp_threadlocal_loc)
            }
            None => muthread_start_normal(new_sp, sp_threadlocal_loc)
        }

        // Thread finished, delete it's data
        Box::from_raw(muthread_ptr);
    }

    std::ptr::null() as *const SysVoid as *mut SysVoid
}

pub(super) unsafe fn sys_thread_join(thread: SysThreadID) -> SysResult {
    libc::pthread_join(
        thread,
        std::ptr::null() as *const c_void as *mut c_void as *mut *mut c_void
    )
}

pub(super) fn sys_thread_self() -> SysThreadID {
    unsafe { libc::pthread_self() }
}
