// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub use super::thread_posix::*;
pub use crate::runtime::thread::thread_rtmu::RTAttr;
use crate::runtime::thread::MuThread;

pub type SysPriority = libc::c_int;
pub type SysTime = libc::timespec;
pub type SysAffinityMask = libc::cpu_set_t;
pub type SysAffinitySize = libc::size_t;
pub type SysSchedPolicy = libc::c_int;

pub const MU_DEFAULT_SCHED_POLICY: SysSchedPolicy = libc::SCHED_FIFO;
pub const MU_DEFAULT_PRIORITY: SysPriority = 1;
pub const MU_DEFAULT_AFF_SIZE: SysAffinitySize =
    libc::CPU_SETSIZE as SysAffinitySize;
const PTHREAD_EXPLICIT_SCHED: libc::c_int = 1;

extern "C" {
    pub fn pthread_attr_setschedparam(
        attr: *mut SysThreadAttr,
        param: *const libc::sched_param
    ) -> SysResult;

    pub fn pthread_attr_setschedpolicy(
        attr: *mut SysThreadAttr,
        policy: SysSchedPolicy
    ) -> SysResult;

    pub fn pthread_attr_getschedpolicy(
        attr: *mut SysThreadAttr,
        policy: *mut SysSchedPolicy
    ) -> SysResult;

    pub fn pthread_attr_setinheritsched(
        attr: *mut SysThreadAttr,
        inheritsched: libc::c_int
    ) -> SysResult;

    pub fn pthread_attr_getschedparam(
        attr: *const SysThreadAttr,
        param: *mut libc::sched_param
    ) -> SysResult;
}

impl RTAttr {
    pub unsafe fn to_sys_thread_attr(&self) -> SysThreadAttr {
        let mut attr: SysThreadAttr = mem::zeroed();
        let res = libc::pthread_attr_init(
            &attr as *const SysThreadAttr as *mut SysThreadAttr
        );
        assert_eq!(res, MU_SUCCESS);

        let res =
            pthread_attr_setinheritsched(&mut attr, PTHREAD_EXPLICIT_SCHED);

        assert_eq!(res, MU_SUCCESS);

        let res = pthread_attr_setschedpolicy(
            &attr as *const SysThreadAttr as *mut SysThreadAttr,
            MU_DEFAULT_SCHED_POLICY
        );

        assert_eq!(res, MU_SUCCESS);

        let mut sch_param: libc::sched_param = mem::zeroed();
        sch_param.sched_priority = self.priority;
        let res = pthread_attr_setschedparam(
            &attr as *const SysThreadAttr as *mut SysThreadAttr,
            &sch_param as *const libc::sched_param
        );
        assert_eq!(res, MU_SUCCESS);

        let res = libc::pthread_attr_setaffinity_np(
            &attr as *const SysThreadAttr as *mut SysThreadAttr,
            self.affinity_size,
            &(self.affinity) as *const SysAffinityMask
        );
        assert_eq!(res, MU_SUCCESS);

        attr
    }
    pub fn from_sys_thread_attr(sys_attr: Box<SysThreadAttr>) -> Box<RTAttr> {
        let mut param: libc::sched_param = unsafe { mem::zeroed() };
        let mut aff: SysAffinityMask = unsafe { mem::zeroed() };
        let sys_attr = Box::into_raw(sys_attr);

        debug!("RTAttr.from_sys_thread_attr ({:#?})", sys_attr);

        let res = unsafe {
            pthread_attr_getschedparam(
                sys_attr,
                &param as *const libc::sched_param as *mut libc::sched_param
            )
        };

        debug!("RTAttr.from_sys_thread_attr.pthread_attr_getschedparam returned ({:#?})", res);

        assert_eq!(res, MU_SUCCESS);

        let res = unsafe {
            libc::pthread_attr_getaffinity_np(
                sys_attr,
                MU_DEFAULT_AFF_SIZE,
                &aff as *const SysAffinityMask as *mut SysAffinityMask
            )
        };
        debug!("RTAttr.from_sys_thread_attr.pthread_attr_getaffinity_np returned ({:#?})", res);
        assert_eq!(res, MU_SUCCESS);

        let rtattr: Box<RTAttr> = Box::new(RTAttr {
            priority: param.sched_priority,
            affinity: aff,
            affinity_size: MU_DEFAULT_AFF_SIZE
        });

        rtattr
    }
}

pub(super) unsafe fn sys_thread_launch(
    muthread: Box<MuThread>,
    attr: *const RTAttr,
    new_sp: Address,
    exception: Option<Address>
) -> SysResult {
    let _sys_thread_id =
        &muthread.sys_thread_id as *const SysThreadID as *mut SysThreadID;
    let exception = {
        if exception.is_none() {
            Address::from_ptr(std::ptr::null() as *const Address)
        } else {
            exception.unwrap()
        }
    };

    let muthread_ptr = Box::into_raw(muthread);
    let muthread_add = Address::from_mut_ptr(muthread_ptr);
    let mut muthread_box = Box::from_raw(muthread_ptr);

    muthread_box.sys_thread_context = &MuThreadContext {
        muthread: muthread_add,
        new_sp,
        exception
    } as *const MuThreadContext
        as *const SysVoid as *mut SysVoid;

    let muthread_ptr = Box::into_raw(muthread_box);

    let res = if attr.is_null() {
        let muthread = Box::from_raw(muthread_ptr);
        let attr = (muthread.rt_attr).to_sys_thread_attr();
        libc::pthread_create(
            _sys_thread_id,
            &(attr) as *const SysThreadAttr,
            _pthread_entry_point,
            muthread_ptr as *mut SysVoid
        )
    } else {
        let attr = Box::from_raw(attr as *mut RTAttr);
        let _sys_attr = attr.to_sys_thread_attr();
        let _attr = Box::into_raw(attr);
        libc::pthread_create(
            _sys_thread_id,
            &_sys_attr as *const SysThreadAttr,
            _pthread_entry_point,
            muthread_ptr as *mut SysVoid
        )
    };
    res
}

pub fn sys_thread_get_attr(native: SysThreadID) -> Box<RTAttr> {
    debug!("sys_thread_get_attr ({:#?})", native);

    let sys_attr: SysThreadAttr = unsafe { std::mem::zeroed() };
    let sys_attr = Box::new(sys_attr);
    let sys_attr = Box::into_raw(sys_attr);
    let _res = unsafe { libc::pthread_getattr_np(native, sys_attr) };

    debug!("sys_thread_get_attr.pthread_getattr_np returned {}", _res);

    RTAttr::from_sys_thread_attr(unsafe { Box::from_raw(sys_attr) })
}

pub fn sys_thread_set_attr(native: SysThreadID, attr: Box<RTAttr>) {
    let sch_param = libc::sched_param {
        sched_priority: attr.priority
    };

    let res = unsafe {
        libc::pthread_setschedparam(
            native,
            MU_DEFAULT_SCHED_POLICY,
            &sch_param as *const libc::sched_param
        )
    };

    debug!(
        "libc::setschedparam({:?}, {:?}, {:?}) returned {:?}",
        native,
        MU_DEFAULT_SCHED_POLICY,
        &sch_param as *const libc::sched_param,
        res
    );

    assert_eq!(res, MU_SUCCESS);

    let res = unsafe {
        libc::pthread_setaffinity_np(
            native,
            attr.affinity_size,
            &(attr.affinity) as *const libc::cpu_set_t
        )
    };

    debug!(
        "libc::setaffinity({:?}, {:?}, {:?}) returned {:?}",
        native,
        attr.affinity_size,
        &attr.affinity as *const libc::cpu_set_t,
        res
    );

    assert_eq!(res, MU_SUCCESS);
}

pub fn sys_thread_set_priority(
    native: SysThreadID,
    priority: SysPriority
) -> SysResult {
    let sch_param = libc::sched_param {
        sched_priority: priority
    };

    unsafe {
        libc::pthread_setschedparam(
            native,
            MU_DEFAULT_SCHED_POLICY,
            &sch_param as *const libc::sched_param
        )
    }
}

pub fn sys_thread_get_priority(
    native: SysThreadID,
    priority: *mut SysPriority
) -> SysResult {
    let sch_param = libc::sched_param {
        sched_priority: MU_DEFAULT_PRIORITY
    };
    let sched_policy = MU_DEFAULT_SCHED_POLICY;

    let res = unsafe {
        libc::pthread_getschedparam(
            native,
            &sched_policy as *const SysSchedPolicy as *mut SysSchedPolicy,
            &sch_param as *const libc::sched_param as *mut libc::sched_param
        )
    };

    unsafe {
        *priority = sch_param.sched_priority as SysPriority;
    }

    res
}

pub fn sys_thread_set_affinity(
    native: SysThreadID,
    affinity_size: SysAffinitySize,
    affinity: SysAffinityMask
) {
    let res = unsafe {
        libc::pthread_setaffinity_np(
            native,
            affinity_size,
            &affinity as *const libc::cpu_set_t
        )
    };

    assert_eq!(res, MU_SUCCESS);
}

pub fn sys_thread_get_affinity(
    native: SysThreadID,
    affinity_size: SysAffinitySize,
    affinity: *mut SysAffinityMask
) -> SysResult {
    unsafe { libc::pthread_getaffinity_np(native, affinity_size, affinity) }
}

pub fn sys_affinity_zero_cpu(affinity: &mut SysAffinityMask) {
    unsafe {
        libc::CPU_ZERO(affinity);
    };
}

/// Allocates a new affinity mask and returns it
pub fn sys_new_affinity() -> SysAffinityMask {
    let mut affinity: libc::cpu_set_t = unsafe { mem::zeroed() };
    unsafe {
        libc::CPU_SET(0, &mut affinity);
    };
    affinity
}

pub fn sys_affinity_set_cpu(cpu: usize, affinity: &mut SysAffinityMask) {
    unsafe {
        libc::CPU_SET(cpu, affinity);
    };
}

pub fn sys_affinity_clear_cpu(cpu: usize, affinity: &mut SysAffinityMask) {
    unsafe {
        libc::CPU_CLR(cpu, affinity);
    };
}

pub fn sys_affinity_isset_cpu(cpu: usize, affinity: &SysAffinityMask) -> bool {
    unsafe { libc::CPU_ISSET(cpu, affinity) }
}

// pub fn sys_affinity_equal(
//     affinity1: &SysAffinityMask,
//     affinity2: &SysAffinityMask,
// ) -> bool {
//     unsafe { libc::CPU_EQUAL(affinity1, affinity2) }
// }
