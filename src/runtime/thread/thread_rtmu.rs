// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// realtime-specific threading stuff goes here
use super::*;

//pub use ast::ir_rt::*;

pub type MuPriority = SysPriority;
pub type MuTime = SysTime;

#[cfg(target_os = "linux")]
pub use super::thread_rtmu_linux::*;

#[repr(C)]
pub struct RTAttr {
    pub priority: SysPriority,
    pub affinity: SysAffinityMask,
    pub affinity_size: SysAffinitySize /*    pub deadline: MuTime, TODO add
                                        * the logic and uncomment */
}

impl RTAttr {
    /// returns a `RTAttr` object with default attributes
    pub fn new() -> RTAttr {
        let attr = RTAttr {
            priority: MU_DEFAULT_PRIORITY,
            affinity: sys_new_affinity(),
            affinity_size: MU_DEFAULT_AFF_SIZE
        };

        attr
    }

    pub fn get_size() -> usize {
        if cfg!(target_arch = "x86_64") {
            //            panic!("RTAttr size is ={} / 8=",
            // std::mem::size_of::<RTAttr>());
            return std::mem::size_of::<RTAttr>() / 8;
        } else {
            //            panic!("RTAttr size is ={} / 4=",
            // std::mem::size_of::<RTAttr>());
            return std::mem::size_of::<RTAttr>() / 4;
        }
    }
}

impl MuThread {
    /// creates and launches a mu thread, returns a JoinHandle and address to
    /// its MuThread structure
    pub(super) fn mu_thread_launch(
        id: MuID,
        attr: *const RTAttr,
        stack: Box<MuStack>,
        user_tls: Address,
        exception: Option<Address>,
        vm: Arc<VM>
    ) -> *mut MuThread {
        let new_sp = stack.sp;

        // FIXME - not all threads need a mutator
        let mut thread: Box<MuThread> =
            Box::new(MuThread::new(id, mm::new_mutator(), stack, user_tls, vm));
        {
            // set mutator for each allocator
            let mutator_ptr = &mut thread.allocator as *mut mm::Mutator;
            thread.allocator.update_mutator_ptr(mutator_ptr);
        }

        // we need to return the pointer, but we cannot send it to other thread
        let muthread_ptr = Box::into_raw(thread);
        let muthread = unsafe { Box::from_raw(muthread_ptr) };

        let res =
            unsafe { sys_thread_launch(muthread, attr, new_sp, exception) };
        assert_eq!(res, MU_SUCCESS);

        muthread_ptr
    }
}

// Creates a new thread
#[no_mangle]
pub unsafe extern "C" fn muentry_new_rt_thread_exceptional(
    attr: *const SysVoid,
    stack: *mut MuStack,
    thread_local: Address,
    exception: Address
) -> *mut MuThread {
    let vm = MuThread::current_mut().vm.clone();
    let muthread = MuThread::mu_thread_launch(
        vm.next_id(),
        attr as *const RTAttr,
        Box::from_raw(stack),
        thread_local,
        Some(exception),
        vm.clone()
    );
    vm.push_join_handle(muthread);
    muthread
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_new() -> *mut RTAttr {
    let attr_box = Box::new(RTAttr::new());
    Box::into_raw(attr_box)
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_delete(attr: *mut SysVoid) {
    Box::from_raw(attr as *mut RTAttr);
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_set_priority(
    attr: *mut SysVoid,
    priority: usize
) {
    debug!("muentry_attr_set_priority ({:#?}, {})", attr, priority);

    let mut attr = Box::from_raw(attr as *mut RTAttr);
    attr.priority = priority as SysPriority;
    Box::into_raw(attr);
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_get_priority(
    attr: *mut SysVoid
) -> usize {
    debug!("muentry_attr_get_priority ({:#?})", attr);
    let attr = Box::from_raw(attr as *mut RTAttr);
    let p = attr.priority as usize;
    Box::into_raw(attr);

    debug!("muentry_attr_get_priority returned ({})", p);

    p
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_set_cpu(attr: *mut SysVoid, cpu: usize) {
    debug!("muentry_attr_set_cpu ({:#?}, {})", attr, cpu);

    let mut attr: Box<RTAttr> = Box::from_raw(attr as *mut RTAttr);
    sys_affinity_set_cpu(cpu, &mut (attr.affinity));
    Box::into_raw(attr);
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_clear_cpu(
    attr: *mut SysVoid,
    cpu: usize
) {
    debug!("muentry_attr_clear_cpu ({:#?}, {})", attr, cpu);

    let mut attr: Box<RTAttr> = Box::from_raw(attr as *mut RTAttr);
    sys_affinity_clear_cpu(cpu, &mut (attr.affinity));
    Box::into_raw(attr);
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_isset_cpu(
    attr: *mut SysVoid,
    cpu: usize
) -> bool {
    debug!("muentry_attr_isset_cpu ({:#?}, {})", attr, cpu);
    let mut attr: Box<RTAttr> = Box::from_raw(attr as *mut RTAttr);
    let res = sys_affinity_isset_cpu(cpu, &mut (attr.affinity));

    debug!("muentry_attr_isset_cpu returned ({})", res);

    Box::into_raw(attr);

    res
}

#[no_mangle]
pub unsafe extern "C" fn muentry_attr_zero_cpu(attr: *mut SysVoid) {
    let mut attr: Box<RTAttr> = Box::from_raw(attr as *mut RTAttr);
    sys_affinity_zero_cpu(&mut (attr.affinity));
    Box::into_raw(attr);
}

// Creates a new thread
#[no_mangle]
pub unsafe extern "C" fn muentry_new_rt_thread_normal(
    attr: *const SysVoid,
    stack: *mut MuStack,
    thread_local: Address
) -> *mut MuThread {
    let vm = MuThread::current_mut().vm.clone();
    let muthread = MuThread::mu_thread_launch(
        vm.next_id(),
        attr as *const RTAttr,
        Box::from_raw(stack),
        thread_local,
        None,
        vm.clone()
    );
    vm.push_join_handle(muthread);
    muthread
}

#[no_mangle]
pub unsafe extern "C" fn muentry_thread_set_attr(
    muthread: *mut MuThread,
    attr: Box<RTAttr>
) {
    let tid = {
        if muthread.is_null() {
            let mut cur_thread = MuThread::current_mut();
            cur_thread.sys_thread_id
        } else {
            (*muthread).sys_thread_id
        }
    };

    sys_thread_set_attr(tid, attr);
}

#[no_mangle]
pub unsafe extern "C" fn muentry_thread_get_attr(
    muthread: *mut MuThread
) -> Box<RTAttr> {
    let tid = {
        if muthread.is_null() {
            let mut cur_thread = MuThread::current_mut();
            cur_thread.sys_thread_id
        } else {
            (*muthread).sys_thread_id
        }
    };

    sys_thread_get_attr(tid)
}

#[no_mangle]
pub unsafe extern "C" fn muentry_thread_set_priority(
    muthread: *mut MuThread,
    priority: u64
) {
    let tid = {
        if muthread.is_null() {
            let mut cur_thread = MuThread::current_mut();
            cur_thread.sys_thread_id
        } else {
            (*muthread).sys_thread_id
        }
    };

    // TODO FIX this
    //    (*muthread).rt_attr.priority = priority as i32;

    if tid != 0 {
        let res = sys_thread_set_priority(tid, priority as MuPriority);
        assert_eq!(res, MU_SUCCESS);
    // println!("mthread_set_priority({}, {}), error #{}#", tid, priority, res);
    } else {
        println!(
            "mthread_set_priority, MuThread #{}# not ready yet",
            (*muthread).hdr.id()
        );
    }
}

#[no_mangle]
pub unsafe extern "C" fn muentry_thread_get_priority(
    muthread: *const MuThread
) -> u64 {
    let priority: MuPriority = mem::zeroed();

    let tid = {
        if muthread.is_null() {
            let mut cur_thread = MuThread::current_mut();
            cur_thread.sys_thread_id
        } else {
            (*muthread).sys_thread_id
        }
    };

    if tid != 0 {
        let res = sys_thread_get_priority(
            tid,
            &priority as *const MuPriority as *mut MuPriority
        );

        if res != MU_SUCCESS {
            warn!("muthread_get_priority failed with error code #{}#", res);
        }

        // println!("mthread_get_priority({}), returns #{}# with error #{}#",
        // tid, priority, res);

        priority as u64
    } else {
        println!(
            "mthread_set_priority, MuThread #{}# not ready yet",
            (*muthread).hdr.id()
        );
        (*muthread).rt_attr.priority as u64
    }
}

pub fn mu_new_affinity() -> SysAffinityMask {
    sys_new_affinity()
}

#[no_mangle]
pub extern "C" fn muentry_thread_set_cpu(muthread: *mut MuThread, cpu: usize) {
    let mut cur_muthread: Box<MuThread> = unsafe {
        Box::from_raw({
            if muthread.is_null() {
                MuThread::current_mut() as *const MuThread as *mut MuThread
            } else {
                muthread
            }
        })
    };
    sys_affinity_set_cpu(cpu, &mut cur_muthread.rt_attr.affinity);
    sys_thread_set_affinity(
        cur_muthread.sys_thread_id,
        MU_DEFAULT_AFF_SIZE,
        cur_muthread.rt_attr.affinity
    );
    let _cur_muthread = Box::into_raw(cur_muthread);
}

#[no_mangle]
pub extern "C" fn muentry_thread_clear_cpu(
    muthread: *mut MuThread,
    cpu: usize
) {
    let mut cur_muthread: Box<MuThread> = unsafe {
        Box::from_raw({
            if muthread.is_null() {
                MuThread::current_mut() as *const MuThread as *mut MuThread
            } else {
                muthread
            }
        })
    };
    sys_affinity_clear_cpu(cpu, &mut cur_muthread.rt_attr.affinity);
    sys_thread_set_affinity(
        cur_muthread.sys_thread_id,
        MU_DEFAULT_AFF_SIZE,
        cur_muthread.rt_attr.affinity
    );

    let _cur_muthread = Box::into_raw(cur_muthread);
}

#[no_mangle]
pub extern "C" fn muentry_thread_isset_cpu(
    muthread: *mut MuThread,
    cpu: usize
) -> bool {
    let mut cur_muthread: Box<MuThread> = unsafe {
        Box::from_raw({
            if muthread.is_null() {
                MuThread::current_mut() as *const MuThread as *mut MuThread
            } else {
                muthread
            }
        })
    };
    let res = sys_affinity_isset_cpu(cpu, &mut cur_muthread.rt_attr.affinity);
    let _cur_muthread = Box::into_raw(cur_muthread);

    res
}
