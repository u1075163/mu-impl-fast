// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

// use std::boxed::Box;
use super::*;

#[cfg(target_os = "linux")]
use super::futex_rtmu_linux::*;

#[no_mangle]
pub extern "C" fn muentry_futex_new() -> Address {
    sys_rtmu_futex::new()
}

#[no_mangle]
pub extern "C" fn muentry_futex_delete(f: Address) {
    sys_rtmu_futex::delete(f);
}

#[no_mangle]
pub extern "C" fn muentry_futex_lock(futex_addr: Address, timeout_ns: u64) {
    let the_futex: &mut sys_rtmu_futex = unsafe { futex_addr.to_ref_mut() };
    the_futex.lock(timeout_ns);
}

#[no_mangle]
pub extern "C" fn muentry_futex_unlock(futex_addr: Address) {
    let the_futex: &mut sys_rtmu_futex = unsafe { futex_addr.to_ref_mut() };
    the_futex.unlock();
}
