// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use super::*;

extern crate libc;

//#[link(name = "runtime_c")]
extern "C" {
    pub fn posix_timer_create(
        tmr_handler: *mut libc::c_void
    ) -> *mut libc::c_void;

    pub fn posix_timer_settime(
        timer_ptr: *mut libc::c_void,
        dur: u64,
        prd: u64
    );

    pub fn posix_timer_cancel(timer_ptr: *mut libc::c_void);
    pub fn posix_timer_delete(timer_ptr: *mut libc::c_void);
}

#[repr(C)]
struct TmrHandler {
    func_ptr: Address,
    arg_ptr: Address
}

pub struct SysTimer {
    timer_ptr: Address,
    handler_ptr: Address
}

impl SysTimer {
    pub(super) fn new() -> *mut SysTimer {
        trace!("SysTimer.NEW");
        let mut stbox = Box::new(SysTimer {
            timer_ptr: unsafe { Address::zero() },
            handler_ptr: {
                Address::from_mut_ptr(Box::into_raw(Box::new(TmrHandler {
                    func_ptr: unsafe { Address::zero() },
                    arg_ptr: unsafe { Address::zero() }
                })))
            }
        });

        stbox.timer_ptr = Address::from_mut_ptr(unsafe {
            posix_timer_create(stbox.handler_ptr.to_ptr_mut())
        });

        // Continue from here
        // use timer_create to initialize stuff
        // then use set-timer and other stuff in `set`, to modify

        let st = Box::into_raw(stbox);
        trace!("__st_ptr: {:#?}", st);

        st
    }

    pub(super) fn set(
        &mut self,
        tm: clock::TimeValT,
        prd: clock::TimeValT,
        func: Address,
        args: Address
    ) {
        trace!("SysTimer.SET ({}, {}, {}, {})", tm, prd, func, args);

        let handler: *mut TmrHandler = self.handler_ptr.to_ptr_mut();
        unsafe {
            (*handler).func_ptr = func;
            (*handler).arg_ptr = args;
        }

        unsafe {
            posix_timer_settime(
                self.timer_ptr.to_ptr_mut(),
                tm as u64,
                prd as u64
            );
        };
    }

    pub(super) fn cancel(&mut self) {
        unsafe {
            posix_timer_cancel(self.timer_ptr.to_ptr_mut());
        };
    }

    pub(super) fn delete(&mut self) {
        unsafe {
            posix_timer_cancel(self.timer_ptr.to_ptr_mut());
            posix_timer_delete(self.timer_ptr.to_ptr_mut());
        }
    }
}
