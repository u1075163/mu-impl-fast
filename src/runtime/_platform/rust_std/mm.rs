// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use super::*;
use std::alloc::{alloc, dealloc, Layout};

pub type SysMemBackStore = *mut u8;

pub fn sys_get_base_addr(ptr: SysMemBackStore) -> Address {
    Address::from_mut_ptr(ptr)
}

pub fn sys_new_raw_mem(size: usize) -> SysMemBackStore {
    sys_allocate_by_size(size)
}

pub fn sys_delete_raw_mem(backstore: SysMemBackStore, size: usize) {
    sys_delete_mem(backstore, size)
}

pub fn sys_ealloc(size: usize) -> Address {
    sys_get_base_addr(sys_allocate_by_size(size))
}

pub fn sys_edelete(backstore: Address, size: usize) {
    sys_delete_mem(backstore.to_ptr_mut() as *const u8 as *mut u8, size)
}

pub fn sys_get_aligned_size(size: usize, align: usize) -> usize {
    if size % align == 0 {
        size
    } else {
        size + (align - (size % align))
    }
}

fn sys_allocate_by_size(size: usize) -> SysMemBackStore {
    let mem_layout =
        Box::new(match Layout::from_size_align(size, POINTER_SIZE) {
            Ok(LO) => LO,
            Err(ERR) => panic!("Allocating region failed with err: {}", ERR)
        });
    unsafe { alloc(*mem_layout) }
}

fn sys_delete_mem(backstore: SysMemBackStore, size: usize) {
    let bbbox = unsafe { Box::from_raw(backstore) };
    let mem_layout =
        Box::new(match Layout::from_size_align(size, POINTER_SIZE) {
            Ok(LO) => LO,
            Err(ERR) => panic!("Allocating region failed with err: {}", ERR)
        });
    unsafe { dealloc(backstore, *mem_layout) };
    let backstore = Box::into_raw(bbbox);
}
