// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::runtime::ValueLocation;

use ast::ir;
use ast::ir::*;
use ast::ptr::*;
use ast::types::*;
use crate::compiler::backend::RegGroup;

use std::sync::{Arc, RwLock};
pub type EntryFuncSig = MuFuncSig;

pub struct RuntimeEntrypoint {
    pub sig: P<MuFuncSig>,
    pub aot: ValueLocation,
    pub jit: RwLock<Option<ValueLocation>>
}

impl RuntimeEntrypoint {
    fn new(
        c_name: &str,
        arg_tys: Vec<P<MuType>>,
        ret_tys: Vec<P<MuType>>
    ) -> RuntimeEntrypoint {
        RuntimeEntrypoint {
            sig: P(MuFuncSig {
                hdr: MuEntityHeader::unnamed(ir::new_internal_id()),
                ret_tys: ret_tys,
                arg_tys: arg_tys
            }),
            aot: ValueLocation::Relocatable(
                RegGroup::GPR,
                Arc::new(c_name.to_string())
            ),
            jit: RwLock::new(None)
        }
    }
}

// decl: thread/mod.rs
//#[cfg(not(feature = "realtime"))]
lazy_static! {
    // impl: runtime_ARCH_OS.c
    pub static ref GET_THREAD_LOCAL : RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_get_thread_local",
        vec![],
        vec![ADDRESS_TYPE.clone()]);
    pub static ref SET_RETVAL : RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_set_retval",
        vec![UINT32_TYPE.clone()],
        vec![]);
    pub static ref THREAD_EXIT : RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_exit",
        vec![ADDRESS_TYPE.clone()],
        vec![]);

    // impl: thread/mod.rs
    pub static ref NEW_STACK: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_new_stack",
        vec![ADDRESS_TYPE.clone(), ADDRESS_TYPE.clone()],
        vec![STACKREF_TYPE.clone()]);
    pub static ref KILL_STACK: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_kill_stack",
        vec![STACKREF_TYPE.clone()],
        vec![]);
    pub static ref SAFECALL_KILL_STACK: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_safecall_kill_stack",
        vec![STACKREF_TYPE.clone()],
        vec![]);
//    pub static ref NEW_THREAD_NORMAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
//        "muentry_new_thread_normal",
//        vec![STACKREF_TYPE.clone(), REF_VOID_TYPE.clone()],
//        vec![THREADREF_TYPE.clone()]);
//    pub static ref NEW_THREAD_EXCEPTIONAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
//        "muentry_new_thread_exceptional",
//        vec![STACKREF_TYPE.clone(), REF_VOID_TYPE.clone(), REF_VOID_TYPE.clone()],
//        vec![THREADREF_TYPE.clone()]);
}

#[cfg(not(feature = "realtime"))]
lazy_static! {
    pub static ref NEW_THREAD_NORMAL: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_new_thread_normal",
            vec![STACKREF_TYPE.clone(), REF_VOID_TYPE.clone()],
            vec![THREADREF_TYPE.clone()]
        );
    pub static ref NEW_THREAD_EXCEPTIONAL: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_new_thread_exceptional",
            vec![
                STACKREF_TYPE.clone(),
                REF_VOID_TYPE.clone(),
                REF_VOID_TYPE.clone()
            ],
            vec![THREADREF_TYPE.clone()]
        );
}

// decl: thread/mod.rs
#[cfg(feature = "realtime")]
lazy_static! {
    pub static ref NEW_RT_THREAD_NORMAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_new_rt_thread_normal",
        vec![ATTRREF_TYPE.clone(), STACKREF_TYPE.clone(), REF_VOID_TYPE.clone()],
        vec![THREADREF_TYPE.clone()]);
    pub static ref NEW_RT_THREAD_EXCEPTIONAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_new_rt_thread_exceptional",
        vec![ATTRREF_TYPE.clone(), STACKREF_TYPE.clone(), REF_VOID_TYPE.clone(), REF_VOID_TYPE.clone()],
        vec![THREADREF_TYPE.clone()]);
    pub static ref THREAD_SET_ATTR: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_set_attr",
        vec![THREADREF_TYPE.clone(), ADDRESS_TYPE.clone()],
        vec![]
    );
    pub static ref THREAD_GET_ATTR: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_get_attr",
        vec![THREADREF_TYPE.clone()],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref THREAD_SET_PRIORITY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_set_priority",
        vec![THREADREF_TYPE.clone(), UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref THREAD_GET_PRIORITY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_get_priority",
        vec![THREADREF_TYPE.clone()],
        vec![UINT64_TYPE.clone()]
    );
    pub static ref THREAD_CLEAR_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_clear_cpu",
        vec![THREADREF_TYPE.clone(), UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref THREAD_SET_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_set_cpu",
        vec![THREADREF_TYPE.clone(), UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref THREAD_ISSET_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_thread_isset_cpu",
        vec![THREADREF_TYPE.clone(), UINT64_TYPE.clone()],
        vec![UINT1_TYPE.clone()]
    );
    /// Allocate an EMM region
    pub static ref NEW_REG: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_new_reg",
        vec![UINT64_TYPE.clone()],      // (region_size)
        vec![ADDRESS_TYPE.clone()]      // returns region_ref
    );
    /// Deallocate an EMM region
    pub static ref DELETE_REG: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_delete_reg",
        vec![ADDRESS_TYPE.clone()],     // (region_ref)
        vec![]                          // returns nothing
    );
    /// Untraced rAlloc for non-hybrid types
    pub static ref RALLOC: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ralloc",
        vec![
            ADDRESS_TYPE.clone(),       // (region_ref,
            UINT64_TYPE.clone()         // block_size)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced rAlloc for non-hybrid types
    pub static ref RALLOC_TRACED: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ralloc_traced",
        vec![
            ADDRESS_TYPE.clone(),       // (region_ref,
            UINT64_TYPE.clone()         // type_id)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced rAlloc for hybrid types
    pub static ref RALLOC_HYBRID: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ralloc_hybrid",
        vec![
            ADDRESS_TYPE.clone(),       // (region_ref,
            UINT64_TYPE.clone(),        // fixed_part_length,
            UINT64_TYPE.clone(),        // var_part_unit_size,
            UINT64_TYPE.clone()         // var_part_length)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced rAlloc for hybrid types
    pub static ref RALLOC_HYBRID_TRACED: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ralloc_hybrid_traced",
        vec![
            ADDRESS_TYPE.clone(),       // (region_ref,
            UINT64_TYPE.clone(),        // hybrid_type_id,
            UINT64_TYPE.clone()         // var_part_length)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Untraced eAlloc for both non-hybrid types
    pub static ref EALLOC: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ealloc",
        vec![
            UINT64_TYPE.clone()         // (block_size)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced eAlloc for non-hybrid types
    pub static ref EALLOC_TRACED: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ealloc_traced",
        vec![
            UINT64_TYPE.clone()         // (type_id)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced eAlloc for hybrid types
    pub static ref EALLOC_HYBRID: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ealloc_hybrid",
        vec![
            UINT64_TYPE.clone(),        // (fixed_part_len,
            UINT64_TYPE.clone(),        // var_part_unit_size,
            UINT64_TYPE.clone()         // var_part_length)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Traced eAlloc for hybrid types
    pub static ref EALLOC_HYBRID_TRACED: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_ealloc_hybrid_traced",
        vec![
            UINT64_TYPE.clone(),        // (hybrid_type_id,
            UINT64_TYPE.clone()         // var_part_length)
        ],
        vec![ADDRESS_TYPE.clone()]      // returns uptr<T>
    );
    /// Delete for all objects allocated by the eAlloc family
    pub static ref EDELETE: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_edelete",
        vec![
            ADDRESS_TYPE.clone()        // (block_uptr)
        ],
        vec![]                          // returns nothing
    );
    /// Get the current time value, since the UNIX_EPOCH, in nano-seconds
    pub static ref GETTIME: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_gettime",
        vec![],
        vec![UINT64_TYPE.clone()]      // returns u128 (#ns)
    );
    /// Set the current time value, in nano-seconds
    pub static ref SETTIME: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_settime",
        vec![UINT64_TYPE.clone()],     // (u128)
        vec![]                          // returns nothing
    );

    pub static ref NEWTIMER: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_new_timer",
        vec![],                         // ()
        vec![ADDRESS_TYPE.clone()]      // returns timerref
    );
    pub static ref SETTIMER: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_set_timer",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone(), UINT64_TYPE.clone(), ADDRESS_TYPE.clone(), ADDRESS_TYPE.clone()],
                                        // (timerref, timeval, ufuncptr, uptr<void>)
        vec![]      // returns timerref
    );
    pub static ref CANCELTIMER: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_cancel_timer",
        vec![ADDRESS_TYPE.clone()],     // (timerref)
        vec![]                          // returns nothing
    );
    pub static ref DELETETIMER: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_delete_timer",
        vec![ADDRESS_TYPE.clone()],     // (timerref)
        vec![]                          // returns nothing
    );
    pub static ref SLEEP: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_sleep_ns",
        vec![UINT64_TYPE.clone()],     // (u64)
        vec![]                          // returns nothing
    );
    pub static ref NEWATTR: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_new",
        vec![],     // ()
        vec![ADDRESS_TYPE.clone()]  // returns attrref
    );
    pub static ref DELETEATTR: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_delete",
        vec![ADDRESS_TYPE.clone()],     // (attrref)
        vec![]  // returns nothing
    );
    pub static ref ATTR_SET_PRIORITY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_set_priority",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone()],     // (attrref, u64)
        vec![]  // returns nothing
    );
    pub static ref ATTR_GET_PRIORITY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_get_priority",
        vec![ADDRESS_TYPE.clone()],     // (attrref)
        vec![UINT64_TYPE.clone()]  // returns u64
    );
    pub static ref ATTR_SET_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_set_cpu",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone()],     // (attrref, u64)
        vec![]  // returns nothing
    );
    pub static ref ATTR_CLEAR_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_clear_cpu",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone()],     // (attrref, u64)
        vec![]  // returns nothing
    );
    pub static ref ATTR_ISSET_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_isset_cpu",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone()],     // (attrref, u64)
        vec![UINT1_TYPE.clone()]  // returns bool
    );
    pub static ref ATTR_ZERO_CPU: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_attr_zero_cpu",
        vec![ADDRESS_TYPE.clone()],     // (attrref)
        vec![]  // returns nothing
    );
}

// impl/decl: gc/lib.rs
lazy_static! {
    pub static ref ALLOC_TINY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_alloc_tiny",
        vec![
            ADDRESS_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref ALLOC_TINY_SLOW: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_alloc_tiny_slow",
        vec![
            ADDRESS_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref ALLOC_NORMAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_alloc_normal",
        vec![
            ADDRESS_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref ALLOC_NORMAL_SLOW: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_alloc_normal_slow",
            vec![
                ADDRESS_TYPE.clone(),
                UINT64_TYPE.clone(),
                UINT64_TYPE.clone()
            ],
            vec![ADDRESS_TYPE.clone()]
        );
    pub static ref ALLOC_LARGE: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_alloc_large",
        vec![
            ADDRESS_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref ALLOC_VAR_SIZE: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_alloc_var_size",
        vec![
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref INIT_TINY: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_init_tiny_object",
        vec![
            ADDRESS_TYPE.clone(),
            ADDRESS_TYPE.clone(),
            UINT8_TYPE.clone()
        ],
        vec![]
    );
    pub static ref INIT_SMALL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_init_small_object",
        vec![
            ADDRESS_TYPE.clone(),
            ADDRESS_TYPE.clone(),
            UINT16_TYPE.clone()
        ],
        vec![]
    );
    pub static ref INIT_MEDIUM: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_init_medium_object",
        vec![
            ADDRESS_TYPE.clone(),
            ADDRESS_TYPE.clone(),
            UINT32_TYPE.clone()
        ],
        vec![]
    );
    pub static ref INIT_LARGE: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_init_large_object",
        vec![
            ADDRESS_TYPE.clone(),
            ADDRESS_TYPE.clone(),
            UINT64_TYPE.clone(),
            UINT64_TYPE.clone()
        ],
        vec![]
    );
    pub static ref PIN_OBJECT: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_pin_object",
        vec![ADDRESS_TYPE.clone()],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref UNPIN_OBJECT: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_unpin_object",
        vec![ADDRESS_TYPE.clone()],
        vec![]
    );
}

// decl: futex
lazy_static! {
    pub static ref NEW_FUTEX: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_futex_new",
        vec![],
        vec![ADDRESS_TYPE.clone()]
    );
    pub static ref DELETE_FUTEX: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_futex_delete",
        vec![ADDRESS_TYPE.clone()],
        vec![]
    );
    pub static ref LOCK_FUTEX: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_futex_lock",
        vec![ADDRESS_TYPE.clone(), UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref UNLOCK_FUTEX: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_futex_unlock",
        vec![ADDRESS_TYPE.clone()],
        vec![]
    );
}

// decl: exception.rs
lazy_static! {
    // impl: exception.rs
    pub static ref THROW_EXCEPTION : RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_throw_exception",
        vec![ADDRESS_TYPE.clone()],
        vec![]);
    // impl: runtime_ARCH_OS.S
    pub static ref THROW_EXCEPTION_INTERNAL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "throw_exception_internal",
        vec![ADDRESS_TYPE.clone(), ADDRESS_TYPE.clone()],
        vec![]);
}

// impl/decl: math.rs
lazy_static! {
    pub static ref FREM32: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_frem32",
        vec![FLOAT_TYPE.clone(), FLOAT_TYPE.clone()],
        vec![FLOAT_TYPE.clone()]
    );
    pub static ref FREM64: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_frem64",
        vec![DOUBLE_TYPE.clone(), DOUBLE_TYPE.clone()],
        vec![DOUBLE_TYPE.clone()]
    );
    pub static ref UDIV_U128: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_udiv_u128",
        vec![UINT128_TYPE.clone(), UINT128_TYPE.clone()],
        vec![UINT128_TYPE.clone()]
    );
    pub static ref SDIV_I128: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_sdiv_i128",
        vec![UINT128_TYPE.clone(), UINT128_TYPE.clone()],
        vec![UINT128_TYPE.clone()]
    );
    pub static ref UREM_U128: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_urem_u128",
        vec![UINT128_TYPE.clone(), UINT128_TYPE.clone()],
        vec![UINT128_TYPE.clone()]
    );
    pub static ref SREM_I128: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_srem_i128",
        vec![UINT128_TYPE.clone(), UINT128_TYPE.clone()],
        vec![UINT128_TYPE.clone()]
    );
    pub static ref FPTOUI_DOUBLE_U128: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_fptoui_double_u128",
            vec![DOUBLE_TYPE.clone()],
            vec![UINT128_TYPE.clone()]
        );
    pub static ref FPTOSI_DOUBLE_I128: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_fptosi_double_i128",
            vec![DOUBLE_TYPE.clone()],
            vec![UINT128_TYPE.clone()]
        );
    pub static ref UITOFP_U128_DOUBLE: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_uitofp_u128_double",
            vec![UINT128_TYPE.clone()],
            vec![DOUBLE_TYPE.clone()]
        );
    pub static ref SITOFP_I128_DOUBLE: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_sitofp_i128_double",
            vec![UINT128_TYPE.clone()],
            vec![DOUBLE_TYPE.clone()]
        );
    pub static ref FPTOUI_FLOAT_U128: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_fptoui_float_u128",
            vec![FLOAT_TYPE.clone()],
            vec![UINT128_TYPE.clone()]
        );
    pub static ref FPTOSI_FLOAT_I128: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_fptosi_float_i128",
            vec![FLOAT_TYPE.clone()],
            vec![UINT128_TYPE.clone()]
        );
    pub static ref UITOFP_U128_FLOAT: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_uitofp_u128_float",
            vec![UINT128_TYPE.clone()],
            vec![FLOAT_TYPE.clone()]
        );
    pub static ref SITOFP_I128_FLOAT: RuntimeEntrypoint =
        RuntimeEntrypoint::new(
            "muentry_sitofp_i128_float",
            vec![UINT128_TYPE.clone()],
            vec![FLOAT_TYPE.clone()]
        );
}

// impl/decl: mod.rs
lazy_static! {
    pub static ref PRINT_HEX: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_print_hex",
        vec![UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref PRINT_BOOL: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_print_bool",
        vec![UINT1_TYPE.clone()],
        vec![]
    );
    pub static ref PRINT_TIME: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_print_time",
        vec![UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref MEM_ZERO: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_mem_zero",
        vec![IREF_VOID_TYPE.clone(), UINT64_TYPE.clone()],
        vec![]
    );
    pub static ref RAND_INT64: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_rand_int64",
        vec![UINT64_TYPE.clone(), UINT64_TYPE.clone()],
        vec![UINT64_TYPE.clone()]
    );
    pub static ref RAND_F64: RuntimeEntrypoint = RuntimeEntrypoint::new(
        "muentry_rand_f64",
        vec![DOUBLE_TYPE.clone(), DOUBLE_TYPE.clone()],
        vec![DOUBLE_TYPE.clone()]
    );
}
