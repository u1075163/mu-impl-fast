// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_mu_rand_i() {
    build_and_run_test!(func, new_rand_i_tester1, new_rand_i);
}

fn new_rand_i() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) int64            = mu_int(64));

    funcsig!    ((vm) sig = (int64, int64) -> (int64));

    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    block!      ((vm, func_v1) blk_entry);

    ssa!        ((vm, func_v1) <int64> min);
    ssa!        ((vm, func_v1) <int64> max);
    ssa!        ((vm, func_v1) <int64> res);

    inst!       ((vm, func_v1) blk_entry_rand:
        res = RANDI min, max
    );

    inst!       ((vm, func_v1) blk_entry_print_rand:
        PRINTHEX res
    );

    inst!       ((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!   ((vm, func_v1) blk_entry(min, max) {
        blk_entry_rand, blk_entry_print_rand,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {blk_entry});

    emit_test!((vm)
        TESTER  new_rand_i_tester1,
        TESTEE  func,
        INPUTS  int64(0), int64(0xFF),
        EXPECTED    ULT     int64(0xFF),
    );

    vm
}

#[test]
fn test_rt_mu_rand_f() {
    build_and_run_test!(func, new_rand_f_tester1, new_rand_f);
}

fn new_rand_f() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) f64_t            = mu_double);

    funcsig!    ((vm) sig = (f64_t, f64_t) -> (f64_t));

    funcdecl!   ((vm) <sig> func);
    funcdef!    ((vm) <sig> func VERSION func_v1);

    block!      ((vm, func_v1) blk_entry);

    ssa!        ((vm, func_v1) <f64_t> min);
    ssa!        ((vm, func_v1) <f64_t> max);
    ssa!        ((vm, func_v1) <f64_t> res);

    inst!       ((vm, func_v1) blk_entry_rand:
        res = RANDF min, max
    );

    //    inst!       ((vm, func_v1) blk_entry_print_rand:
    //        PRINTHEX res
    //    );

    inst!       ((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!   ((vm, func_v1) blk_entry(min, max) {
        blk_entry_rand,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {blk_entry});

    emit_test!((vm)
        TESTER  new_rand_f_tester1,
        TESTEE  func,
        INPUTS  f64_t(0.0), f64_t(1.0),
        EXPECTED    FULT     f64_t(1.0),
    );

    vm
}
