// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::BinOp;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_get_time() {
    build_and_run_test!(func, get_time_tester_1, get_time);
}
#[no_mangle]
fn get_time() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int128  = mu_int(64));

    funcsig!((vm) sig = () -> (int128));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int128> res);

    inst_rt!((vm, func_v1) blk_entry_get_time:
        res = GETTIME
    );

    inst!((vm, func_v1) blk_entry_print_time:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_get_time,
        blk_entry_print_time,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  get_time_tester_1,
        TESTEE  func,
        EXPECTED    NE  int128(0),
    );

    vm
}

#[test]
fn test_rt_set_time() {
    build_and_run_test!(func, set_time_tester_2, set_time);
    build_and_run_test!(func, set_time_tester_1, set_time);
}
#[no_mangle]
fn set_time() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int128  = mu_int(64));

    funcsig!((vm) sig = (int128) -> (int128));
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    ssa!((vm, func_v1) <int128> _timeval);
    ssa!((vm, func_v1) <int128> res);

    inst_rt!((vm, func_v1) blk_entry_set_time:
        SETTIME     _timeval
    );

    inst_rt!((vm, func_v1) blk_entry_get_time:
        res = GETTIME
    );

    inst!((vm, func_v1) blk_entry_print_time:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_ret:
        RET (res)
    );

    define_block!((vm, func_v1) blk_entry(_timeval) {
        blk_entry_set_time,
        blk_entry_get_time,
        blk_entry_print_time,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  set_time_tester_1,
        TESTEE  func,
        INPUTS  int128(0),
        EXPECTED    NE  int128(0),
    );
    emit_test!((vm)
        TESTER  set_time_tester_2,
        TESTEE  func,
        INPUTS  int128(0x5FFFFFFF),
        EXPECTED    NE  int128(0),
    );

    vm
}

#[test]
fn test_rt_set_timer() {
    build_and_run_test!(
        VM_BUILDER      set_timer(),
        DEP_FUNCS       (func, pp),
        TESTER_NAME     set_timer_tester,
    );
}
#[no_mangle]
fn set_timer() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm)   int64       =   mu_int(64));
    typedef!    ((vm)   arg_t       =   mu_struct(int64));
    typedef!    ((vm)   timerref    =   mu_timerref);
    typedef!    ((vm)   i64_ptr     =   mu_uptr(int64));
    typedef!    ((vm)   arg_ptr     =   mu_uptr(arg_t));

    funcsig!    ((vm)   pp_sig  =   (arg_ptr) -> ());
    funcdecl!   ((vm)   <pp_sig>    pp); // periodic print
    funcdef!    ((vm)   <pp_sig>    pp  VERSION     pp_v1);

    typedef!    ((vm)   ppf_ptr =   mu_funcref(pp_sig));
    constdef!   ((vm)   <ppf_ptr>   const_ppf_ptr   =   Constant::FuncRef(pp)); // pointer to periodic print
    constdef!   ((vm)   <int64>     const_start_time    =   Constant::Int(500_000_000)); // 0.5 second
    constdef!   ((vm)   <int64>     const_period    =   Constant::Int(100_000_000)); // 0.1 second
    constdef!   ((vm)   <int64>     const_test_length    =   Constant::Int(2_000_000_000)); // 2 second
    constdef!   ((vm)   <i64_ptr>   const_null_ptr  =   Constant::NullRef); // Null argument

    // blk_entry
    block!      ((vm, pp_v1)    blk_entry);

    ssa!        ((vm, pp_v1)    <arg_ptr>   last_arg_ptr);

    ssa!        ((vm, pp_v1)    <i64_ptr>   last_tm_ptr);
    ssa!        ((vm, pp_v1)    <int64>     last_tm);
    ssa!        ((vm, pp_v1)    <int64>     tm);
    ssa!        ((vm, pp_v1)    <int64>     duration);

    /*
    BLOCK ENTRY (tmr, period, pp_func, uptr<arg>):
        new_tm  =   GETTIME
        new_arg =   struct(new_tm)
        SETTIMER    tmr, period, pp_func, uptr<new_arg>
        tm = arg.0
        PRINTHEX    new_tm  -   tm      // the real period achieved
    */
    inst_rt!    ((vm, pp_v1) blk_entry_get_time:
        tm = GETTIME
    );

    inst!       ((vm, pp_v1) blk_entry_get_last_tm_ptr:
        last_tm_ptr  =   GETFIELDIREF    last_arg_ptr (is_ptr: true, index: 0)
    );
    inst!       ((vm, pp_v1) blk_entry_load_last:
        last_tm = LOAD last_tm_ptr (is_ptr: true, order: MemoryOrder::NotAtomic)
    );

    inst!       ((vm, pp_v1)    blk_entry_calc_duration:
        duration    =   BINOP (BinOp::Sub) tm last_tm
    );
    inst_rt!    ((vm, pp_v1) blk_entry_print_time:
        PRINTTIME duration
    );

    inst!       ((vm, pp_v1) blk_entry_ret:
        RET
    );

    define_block!((vm, pp_v1) blk_entry(last_arg_ptr) {
        blk_entry_get_time,
        blk_entry_get_last_tm_ptr,
        blk_entry_load_last,
        blk_entry_calc_duration,
        blk_entry_print_time,
        blk_entry_ret
    });

    define_func_ver!((vm) pp_v1 (entry: blk_entry) {
        blk_entry
    });

    funcsig!    ((vm)   func_sig  =   () -> ());
    funcdecl!   ((vm)   <func_sig>  func); // periodic print
    funcdef!    ((vm)   <func_sig>  func  VERSION     func_v1);

    consta!     ((vm, func_v1)  ppf_ptr_local   =   const_ppf_ptr);
    consta!     ((vm, func_v1)  period_local    =   const_period);
    consta!     ((vm, func_v1)  start_time_local    =   const_start_time);
    consta!     ((vm, func_v1)  test_length_local    =   const_test_length);

    ssa!        ((vm, func_v1)  <timerref>  tmr);
    ssa!        ((vm, func_v1)  <int64>     tm);
    ssa!        ((vm, func_v1)  <arg_ptr>   new_arg_ptr);
    ssa!        ((vm, func_v1)  <i64_ptr>   new_tm_ptr);

    // blk_entry
    block!      ((vm, func_v1)  blk_entry);

    inst_rt!    ((vm, func_v1)  blk_entry_get_time:
        tm = GETTIME
    );

    inst_rt!    ((vm, func_v1)  blk_entry_ealloc_arg:
        new_arg_ptr =   EALLOC  arg_t
    );
    inst!       ((vm, func_v1)  blk_entry_get_new_tm_ptr:
        new_tm_ptr  =   GETFIELDIREF    new_arg_ptr (is_ptr: true, index: 0)
    );
    inst!       ((vm, func_v1)  blk_entry_store_new_tm:
        STORE   new_tm_ptr  tm  (is_ptr: true, order: MemoryOrder::SeqCst)
    );

    inst_rt!    ((vm, func_v1)  blk_entry_create_timer:
        tmr = NEWTIMER
    );

    inst!    ((vm, func_v1)  blk_entry_print_func:
        PRINTHEX tmr
    );

    inst_rt!    ((vm, func_v1)  blk_entry_set_next_timer:
        SETTIMER    tmr, start_time_local, period_local, ppf_ptr_local, new_arg_ptr
    );

    inst_rt!    ((vm, func_v1)  blk_entry_print_time:
        PRINTTIME tm
    );

    inst_rt!    ((vm, func_v1)  blk_entry_sleep_once:
        SLEEP_NS test_length_local
    );

    inst_rt!    ((vm, func_v1)  blk_entry_sleep_twice:
        SLEEP_NS test_length_local
    );

    inst!       ((vm, func_v1)  blk_entry_ret:
        RET
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_get_time,
        blk_entry_ealloc_arg,
        blk_entry_get_new_tm_ptr,
        blk_entry_store_new_tm,
        blk_entry_create_timer,
        blk_entry_print_func,
        blk_entry_set_next_timer,
        blk_entry_print_time,
        blk_entry_sleep_once,
        blk_entry_sleep_twice,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  set_timer_tester,
        TESTEE  func,
    );

    vm
}
