// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::CmpOp;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_futex() {
    build_and_run_test!(func, futex_tester_1, futex_ops);
}
#[no_mangle]
fn futex_ops() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!((vm) int64         = mu_int(64));
    typedef!((vm) futexref      = mu_futexref);

    constdef!((vm) <int64> i64_zero    =   Constant::Int(0));

    funcsig!((vm) sig = () -> ());
    funcdecl!((vm) <sig> func);
    funcdef!((vm) <sig> func VERSION func_v1);

    // blk_entry
    block!((vm, func_v1) blk_entry);

    consta!((vm, func_v1)  zero_local   =   i64_zero);
    ssa!((vm, func_v1) <futexref> res);

    inst!((vm, func_v1) blk_entry_new_futex:
        res = NEWFUTEX
    );

    inst!((vm, func_v1) blk_entry_print_futex:
        PRINTHEX res
    );

    inst!((vm, func_v1) blk_entry_lock_futex:
        LOCKFUTEX   res, zero_local
    );

    inst!((vm, func_v1) blk_entry_unlock_futex:
        UNLOCKFUTEX   res
    );

    inst!((vm, func_v1) blk_entry_delete_futex:
        DELETEFUTEX   res
    );

    inst!((vm, func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, func_v1) blk_entry() {
        blk_entry_new_futex,
        blk_entry_print_futex,
        blk_entry_lock_futex,
        blk_entry_unlock_futex,
        blk_entry_delete_futex,
        blk_entry_ret
    });

    define_func_ver!((vm) func_v1 (entry: blk_entry) {
        blk_entry
    });

    emit_test!((vm)
        TESTER  futex_tester_1,
        TESTEE  func,
    );

    vm
}
