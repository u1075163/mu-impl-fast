// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_new_rtattr() {
    build_and_run_test!(new_rtattr, new_rtattr_test1);
}

fn new_rtattr() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) attrref_t         = mu_attrref);

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> new_rtattr);
    funcdef!    ((vm) <sig> new_rtattr VERSION new_rtattr_v1);

    block!      ((vm, new_rtattr_v1) blk_entry);

    // a = NEW <rtattr>
    ssa!        ((vm, new_rtattr_v1) <attrref_t> a);
    inst_rt!       ((vm, new_rtattr_v1) blk_entry_new1:
        a = NEWATTR
    );

    inst!       ((vm, new_rtattr_v1) blk_entry_print1:
        PRINTHEX a
    );

    inst!       ((vm, new_rtattr_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, new_rtattr_v1) blk_entry() {
        blk_entry_new1,
        blk_entry_print1,
        blk_entry_threadexit
    });

    define_func_ver!((vm) new_rtattr_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        new_rtattr, new_rtattr_test1, new_rtattr_test1_v1,
        sig,
    );

    vm
}
