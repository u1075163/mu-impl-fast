// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_rt_regionref() {
    build_and_run_test!(new_regionref, new_regionref_test1);
}

fn new_regionref() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    typedef!    ((vm) regionref_t         = mu_regionref);
    typedef!    ((vm) ref_regionref_t     = mu_ref(regionref_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> new_regionref);
    funcdef!    ((vm) <sig> new_regionref VERSION new_regionref_v1);

    block!      ((vm, new_regionref_v1) blk_entry);

    // a = NEW <regionref>
    ssa!        ((vm, new_regionref_v1) <ref_regionref_t> a);
    inst!       ((vm, new_regionref_v1) blk_entry_new1:
        a = NEW <regionref_t>
    );

    inst!       ((vm, new_regionref_v1) blk_entry_print1:
        PRINTHEX a
    );

    inst!       ((vm, new_regionref_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, new_regionref_v1) blk_entry() {
        blk_entry_new1, blk_entry_print1,
        blk_entry_threadexit
    });

    define_func_ver!((vm) new_regionref_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        new_regionref, new_regionref_test1, new_regionref_test1_v1,
        sig,
    );

    vm
}
