// Copyright 2019 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use self::mu::linkutils;
use self::mu::linkutils::aot;
use std::sync::Arc;

#[test]
fn test_new_rt_thread() {
    VM::start_logging_trace();

    let vm = Arc::new(create_thread());

    let compiler = Compiler::new(CompilerPolicy::default(), &vm);

    let func_id = vm.id_of("the_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    let func_id = vm.id_of("create_thread_func");
    {
        let funcs = vm.funcs().read().unwrap();
        let func = funcs.get(&func_id).unwrap().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();
        let mut func_ver = func_vers
            .get(&func.cur_ver.unwrap())
            .unwrap()
            .write()
            .unwrap();

        compiler.compile(&mut func_ver);
    }

    vm.set_primordial_thread(func_id, true, vec![]);
    backend::emit_context(&vm);

    let executable = aot::link_primordial(
        vec![
            Arc::new("create_thread_func".to_string()),
            Arc::new("the_func".to_string()),
        ],
        "create_thread_func_test",
        &vm
    );
    linkutils::exec_path(executable);
}

fn create_thread() -> VM {
    let vm = VM::new_with_opts("--aot-link-static");

    // 1. define a simple function which prints 0 to screen
    typedef!    ((vm) int64  = mu_int(64));

    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> the_func);
    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);

    consta!     ((vm, the_func_v1) int64_0_local = int64_0);

    block!      ((vm, the_func_v1) blk_entry);

    inst!((vm, the_func_v1) blk_entry_print:
        PRINTHEX int64_0_local
    );

    inst!((vm, the_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!   ((vm, the_func_v1) blk_entry() {
        blk_entry_print,
        blk_entry_ret
    });

    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
        blk_entry
    });

    // 2.1-create a stack on the function from 1.
    // 2.2-create a thread on the stack from 2.1.
    typedef!    ((vm) attrref_t         = mu_attrref);
    typedef!    ((vm) func_ref = mu_funcref(sig));
    typedef!    ((vm) stack_ref = mu_stackref);
    typedef!    ((vm) thread_ref = mu_threadref);

    constdef!   ((vm) <func_ref> const_funcref = Constant::FuncRef(the_func));
    constdef!   ((vm) <attrref_t> attrref_null = Constant::NullRef);

    funcsig!    ((vm) create_thread_sig = () -> ());
    funcdecl!   ((vm) <create_thread_sig> create_thread_func);
    funcdef!    ((vm) <create_thread_sig> create_thread_func VERSION create_thread_func_v1);

    ssa!        ((vm, create_thread_func_v1) <stack_ref> the_stack_ref);
    ssa!        ((vm, create_thread_func_v1) <thread_ref> the_thread_ref);
    // ssa!        ((vm, create_thread_func_v1) <uptr_rtattr_t> the_attr_uptr);
    consta!     ((vm, create_thread_func_v1) const_funcref_local = const_funcref);
    consta!     ((vm, create_thread_func_v1) attrref_null_local = attrref_null);

    // blk_entry
    block!      ((vm, create_thread_func_v1) blk_entry);

    inst!((vm, create_thread_func_v1) blk_entry_create_stack:
        the_stack_ref = NEWSTACK const_funcref_local
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_stack_print:
        PRINTHEX the_stack_ref
    );

    // inst!((vm, create_thread_func_v1) blk_entry_pin_null:
    //     the_attr_uptr = PIN const_nullref_local
    // );

    inst_rt!((vm, create_thread_func_v1) blk_entry_create_thread:
        the_thread_ref = NEWRTTHREAD STACK: the_stack_ref ATTR: attrref_null_local
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_thread_print:
        PRINTHEX the_thread_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, create_thread_func_v1) blk_entry() {
        blk_entry_create_stack,
        blk_entry_create_stack_print,
        // blk_entry_pin_null,
        blk_entry_create_thread,
        blk_entry_create_thread_print,
        blk_entry_ret
    });

    define_func_ver!((vm) create_thread_func_v1 (entry: blk_entry) {
        blk_entry
    });

    vm
}
